module.exports = {
    block: 'page',
    title: 'Новости фонда',
    content: [
        require('./common/navigation.bemjson.js'),
        {
            block: 'header',
            tag: 'header',
            mods: {border: true},
            content: [
                require('./common/header.bemjson.js'),
            ],
        },
        require('./common/breadcrumbs.bemjson.js'),
        {
            block: 'main',
            mods: {border: true},
            content: {
                cls: 'container',
                content: [
                    require('./common/sidebar.bemjson.js'),
                    {
                        elem: 'content',
                        content: [
                            {
                                elem: 'header-wrapper',
                                content: {
                                    elem: 'header',
                                    content: 'Новости фонда',
                                },
                            },
                            {
                                elem: 'news-wrapper',
                                content: [
                                    {
                                        elem: 'news-date-wrapper',
                                        content: [
                                            {
                                                elem: 'news-date',
                                                content: [
                                                    {
                                                        block: 'icon',
                                                        cls: 'icon-big_' +
                                                        'triangle',
                                                    },
                                                    {
                                                        elem: 'news-day-letter',
                                                        content: [
                                                            {
                                                                elem: 'news-' +
                                                                'day',
                                                                content: '29',
                                                            },
                                                            {
                                                                tag: 'span',
                                                                content: '12.' +
                                                                '17',
                                                            },
                                                        ],
                                                    },
                                                ],
                                            },
                                            {
                                                block: 'card',
                                                mods: {hor: true},
                                                content: [
                                                    {
                                                        elem: 'img',
                                                        content: [
                                                            {
                                                                block: 'img',
                                                                src: './imag' +
                                                                'es/media/' +
                                                                '1.jpg',
                                                            },
                                                        ],
                                                    },
                                                    {
                                                        elem: 'body',
                                                        content: [
                                                            {
                                                                elem: 'title',
                                                                content: 'Ден' +
                                                                'ь рождения' +
                                                                ' ВолГУ',
                                                            },
                                                            {
                                                                elem: 'text',
                                                                content: 'В р' +
                                                                'амках реализ' +
                                                                'ации Концепц' +
                                                                'ии общенацио' +
                                                                'нальной сист' +
                                                                'емы выявлени' +
                                                                'я и развития' +
                                                                ' молодых тал' +
                                                                'антов Волгог' +
                                                                'радский госу' +
                                                                'дарственный ' +
                                                                'университет ' +
                                                                'выступил с и' +
                                                                'нициативой с' +
                                                                'оздания в Во' +
                                                                'лгоградской ' +
                                                                'области комп' +
                                                                'лексного про' +
                                                                'екта',
                                                            },
                                                        ],
                                                    },
                                                ],
                                            },
                                            {
                                                block: 'card',
                                                mods: {hor: true},
                                                content: [
                                                    {
                                                        elem: 'img',
                                                        content: [
                                                            {
                                                                block: 'img',
                                                                src: './image' +
                                                                's/media/' +
                                                                '2.jpg',
                                                            },
                                                        ],
                                                    },
                                                    {
                                                        elem: 'body',
                                                        content: [
                                                            {
                                                                elem: 'title',
                                                                content: 'Пре' +
                                                                'дметные ' +
                                                                'кружки для ' +
                                                                'школьников',
                                                            },
                                                            {
                                                                elem: 'text',
                                                                content: 'Цел' +
                                                                'ью проекта ' +
                                                                'является объ' +
                                                                'единение ' +
                                                                'усилий образ' +
                                                                'овательных и' +
                                                                ' общественны' +
                                                                'х организаци' +
                                                                'й, органов и' +
                                                                'сполнительно' +
                                                                'й власти, за' +
                                                                'интересованн' +
                                                                'ых в развити' +
                                                                'и одарённых ' +
                                                                'детей и тала' +
                                                                'нтливой моло' +
                                                                'дёжи, в подг' +
                                                                'отовке ее к ' +
                                                                'созидательно' +
                                                                'й деятельнос' +
                                                                'ти на благо',
                                                            },
                                                        ],
                                                    },
                                                ],
                                            },
                                            {
                                                block: 'card',
                                                mods: {hor: true},
                                                attrs: {'data-type': 'video'},
                                                content: [
                                                    {
                                                        elem: 'img',
                                                        content: [
                                                            {
                                                                block: 'img',
                                                                src: './image' +
                                                                's/media/3.jpg',
                                                            },
                                                            {
                                                                elem: 'type',
                                                                elemMods: {
                                                                    video: true,
                                                                },
                                                                mix: {
                                                                    block:
                                                                        'icon',
                                                                },
                                                                cls: 'icon-tr' +
                                                                'iangle_right',
                                                            },
                                                        ],
                                                    },
                                                    {
                                                        elem: 'body',
                                                        content: [
                                                            {
                                                                elem: 'title',
                                                                content: 'Оли' +
                                                                'мпийская сме' +
                                                                'на',
                                                            },
                                                            {
                                                                elem: 'text',
                                                                content: 'Орг' +
                                                                'анизационно' +
                                                                ' университет' +
                                                                'ский округ с' +
                                                                'остоит из ' +
                                                                'кластеров (г' +
                                                                'руппы образо' +
                                                                'вательных ор' +
                                                                'ганизаций об' +
                                                                'щего, средне' +
                                                                'го профессио' +
                                                                'нального и д' +
                                                                'ополнительно' +
                                                                'го образован' +
                                                                'ия), объедин' +
                                                                'енных вокруг' +
                                                                ' опорной орг' +
                                                                'анизации, в ' +
                                                                'роли которой' +
                                                                ' может высту' +
                                                                'пать школа, ' +
                                                                'гимназия, ли' +
                                                                'цей. Организ' +
                                                                'ационно унив' +
                                                                'ерситетский ' +
                                                                'округ состои' +
                                                                'т из кластер' +
                                                                'ов (группы о' +
                                                                'бразовательн' +
                                                                'ых организац' +
                                                                'ий общего, с' +
                                                                'реднего проф' +
                                                                'ессиональног' +
                                                                'о и дополнит' +
                                                                'ельного обра' +
                                                                'зования), об' +
                                                                'ъединенных в' +
                                                                'округ опорно' +
                                                                'й организаци' +
                                                                'и, в роли ко' +
                                                                'торой может ' +
                                                                'выступать шк' +
                                                                'ола, гимнази' +
                                                                'я, лицей.',
                                                            },
                                                        ],
                                                    },
                                                ],
                                            },
                                        ],
                                    },
                                    {
                                        elem: 'news-date-wrapper',
                                        content: [
                                            {
                                                elem: 'news-date',
                                                content: [
                                                    {
                                                        block: 'icon',
                                                        cls: 'icon-big_' +
                                                        'triangle',
                                                    },
                                                    {
                                                        elem: 'news-day-letter',
                                                        content: [
                                                            {
                                                                elem: 'news-' +
                                                                'day',
                                                                content: '26',
                                                            },
                                                            {
                                                                tag: 'span',
                                                                content: '12.' +
                                                                '17',
                                                            },
                                                        ],
                                                    },
                                                ],
                                            },
                                            {
                                                block: 'card',
                                                mods: {hor: true},
                                                content: [
                                                    {
                                                        elem: 'img',
                                                        content: [
                                                            {
                                                                block: 'img',
                                                                src: './imag' +
                                                                'es/media/' +
                                                                '1.jpg',
                                                            },
                                                        ],
                                                    },
                                                    {
                                                        elem: 'body',
                                                        content: [
                                                            {
                                                                elem: 'title',
                                                                content: 'Ден' +
                                                                'ь рождения' +
                                                                ' ВолГУ',
                                                            },
                                                            {
                                                                elem: 'text',
                                                                content: 'В р' +
                                                                'амках реализ' +
                                                                'ации Концепц' +
                                                                'ии общенацио' +
                                                                'нальной сист' +
                                                                'емы выявлени' +
                                                                'я и развития' +
                                                                ' молодых тал' +
                                                                'антов Волгог' +
                                                                'радский госу' +
                                                                'дарственный ' +
                                                                'университет ' +
                                                                'выступил с и' +
                                                                'нициативой с' +
                                                                'оздания в Во' +
                                                                'лгоградской ' +
                                                                'области комп' +
                                                                'лексного про' +
                                                                'екта',
                                                            },
                                                        ],
                                                    },
                                                ],
                                            },
                                        ],
                                    },
                                    {
                                        elem: 'news-date-wrapper',
                                        content: [
                                            {
                                                elem: 'news-date',
                                                content: [
                                                    {
                                                        block: 'icon',
                                                        cls: 'icon-big_' +
                                                        'triangle',
                                                    },
                                                    {
                                                        elem: 'news-day-letter',
                                                        content: [
                                                            {
                                                                elem: 'news-' +
                                                                'day',
                                                                content: '19',
                                                            },
                                                            {
                                                                tag: 'span',
                                                                content: '12.' +
                                                                '17',
                                                            },
                                                        ],
                                                    },
                                                ],
                                            },
                                            {
                                                block: 'card',
                                                mods: {hor: true},
                                                content: [
                                                    {
                                                        elem: 'img',
                                                        content: [
                                                            {
                                                                block: 'img',
                                                                src: './imag' +
                                                                'es/media/' +
                                                                '2.jpg',
                                                            },
                                                        ],
                                                    },
                                                    {
                                                        elem: 'body',
                                                        content: [
                                                            {
                                                                elem: 'title',
                                                                content: 'Пре' +
                                                                'дметные ' +
                                                                'кружки для ' +
                                                                'школьников',
                                                            },
                                                            {
                                                                elem: 'text',
                                                                content: 'Це' +
                                                                'лью проекта ' +
                                                                'является объ' +
                                                                'единение уси' +
                                                                'лий образова' +
                                                                'тельных и об' +
                                                                'щественных о' +
                                                                'рганизаций, ' +
                                                                'органов испо' +
                                                                'лнительной в' +
                                                                'ласти, заинт' +
                                                                'ересованных ' +
                                                                'в развитии о' +
                                                                'дарённых дет' +
                                                                'ей и талантл' +
                                                                'ивой молодёж' +
                                                                'и, в подгото' +
                                                                'вке ее к соз' +
                                                                'идательной д' +
                                                                'еятельности ' +
                                                                'на благо',
                                                            },
                                                        ],
                                                    },
                                                ],
                                            },
                                            {
                                                block: 'card',
                                                mods: {hor: true},
                                                attrs: {'data-type': 'video'},
                                                content: [
                                                    {
                                                        elem: 'img',
                                                        content: [
                                                            {
                                                                block: 'img',
                                                                src: './imag' +
                                                                'es/media/' +
                                                                '3.jpg',
                                                            },
                                                            {
                                                                elem: 'type',
                                                                elemMods: {
                                                                    video: true,
                                                                },
                                                                mix: {
                                                                    block:
                                                                        'icon',
                                                                },
                                                                cls: 'icon-' +
                                                                'triangle_' +
                                                                'right',
                                                            },
                                                        ],
                                                    },
                                                    {
                                                        elem: 'body',
                                                        content: [
                                                            {
                                                                elem: 'title',
                                                                content: 'Оли' +
                                                                'мпийская сме' +
                                                                'на',
                                                            },
                                                            {
                                                                elem: 'text',
                                                                content: 'Орг' +
                                                                'анизационно' +
                                                                ' университет' +
                                                                'ский округ с' +
                                                                'остоит из ' +
                                                                'кластеров (г' +
                                                                'руппы образо' +
                                                                'вательных ор' +
                                                                'ганизаций об' +
                                                                'щего, средне' +
                                                                'го профессио' +
                                                                'нального и д' +
                                                                'ополнительно' +
                                                                'го образован' +
                                                                'ия), объедин' +
                                                                'енных вокруг' +
                                                                ' опорной орг' +
                                                                'анизации, в ' +
                                                                'роли которой' +
                                                                ' может высту' +
                                                                'пать школа, ' +
                                                                'гимназия, ли' +
                                                                'цей. Организ' +
                                                                'ационно унив' +
                                                                'ерситетский ' +
                                                                'округ состои' +
                                                                'т из кластер' +
                                                                'ов (группы о' +
                                                                'бразовательн' +
                                                                'ых организац' +
                                                                'ий общего, с' +
                                                                'реднего проф' +
                                                                'ессиональног' +
                                                                'о и дополнит' +
                                                                'ельного обра' +
                                                                'зования), об' +
                                                                'ъединенных в' +
                                                                'округ опорно' +
                                                                'й организаци' +
                                                                'и, в роли ко' +
                                                                'торой может ' +
                                                                'выступать шк' +
                                                                'ола, гимнази' +
                                                                'я, лицей.',
                                                            },
                                                        ],
                                                    },
                                                ],
                                            },
                                        ],
                                    },
                                ],
                            },
                            {
                                block: 'btn',
                                mods: {secondary: true, more: true},
                                content: [
                                    'Еще материалы',
                                    {
                                        block: 'icon',
                                        tag: 'i',
                                        cls: 'icon-arr',
                                    },
                                ],
                            },
                            require('./common/pagination.bemjson.js'),
                        ],
                    },
                ],
            },
        },
        require('./common/slider_banners.bemjson.js'),
        require('./common/links.bemjson.js'),
        require('./common/footer.bemjson.js'),
        require('./common/modal_feedback.bemjson.js'),
    ],
};
