module.exports = {
    block: 'page',
    title: 'Медиатека ВолГУ',
    content: [
        require('./common/navigation.bemjson.js'),
        {
            block: 'header',
            tag: 'header',
            mods: {border: true},
            content: [
                require('./common/header.bemjson.js'),
            ],
        },
        require('./common/breadcrumbs.bemjson.js'),
        {
            block: 'main',
            mods: {border: true},
            content: {
                cls: 'container',
                content: [
                    require('./common/sidebar.bemjson.js'),
                    {
                        elem: 'content',
                        content: [
                            {
                                elem: 'header-wrapper',
                                content: [
                                    {
                                        elem: 'header',
                                        content: 'Медиатека',
                                    },
                                    {
                                        elem: 'sort',
                                        content: [
                                            {
                                                block: 'btn',
                                                mods: {sort: true},
                                                attrs: {'data-sort': 'date'},
                                                content: [
                                                    'По дате',
                                                    {
                                                        block: 'icon',
                                                        tag: 'i',
                                                        cls: 'icon-arr',
                                                    },
                                                ],
                                            },
                                            {
                                                block: 'btn',
                                                mods: {sort: true},
                                                attrs: {'data-sort': 'popular'},
                                                content: [
                                                    'По популярности',
                                                    {
                                                        block: 'icon',
                                                        tag: 'i',
                                                        cls: 'icon-arr',
                                                    },
                                                ],
                                            },
                                        ],
                                    },
                                ],
                            },
                            {
                                elem: 'grid',
                                elemMods: {media: true},
                                content: [
                                    {
                                        block: 'card',
                                        attrs: {
                                            'data-type': 'photo',
                                            'data-toggle': 'modal',
                                        },
                                        content: [
                                            {
                                                elem: 'img',
                                                content: [
                                                    {
                                                        block: 'img',
                                                        src: './images/media/' +
                                                        '1.jpg',
                                                    },
                                                    {
                                                        elem: 'type',
                                                        elemMods: {photo: true},
                                                        content: '114 фото',
                                                    },
                                                ],
                                            },
                                            {
                                                elem: 'body',
                                                content: [
                                                    {
                                                        elem: 'date',
                                                        content: '24.05.2012',
                                                    },
                                                    {
                                                        elem: 'title',
                                                        content: 'День ' +
                                                        'рождения ВолГУ',
                                                    },
                                                    {
                                                        elem: 'text',
                                                        content: 'В рамках ре' +
                                                        'ализации Концепции о' +
                                                        'бщенациональной сист' +
                                                        'емы выявления и разв' +
                                                        'ития молодых таланто' +
                                                        'в Волгоградский госу' +
                                                        'дарственный универси' +
                                                        'тет выступил с иници' +
                                                        'ативой создания в Во' +
                                                        'лгоградской области ' +
                                                        'комплексного проекта',
                                                    },
                                                ],
                                            },
                                        ],
                                    },
                                    {
                                        block: 'card',
                                        attrs: {
                                            'data-type': 'video',
                                            'data-toggle': 'modal',
                                        },
                                        content: [
                                            {
                                                elem: 'img',
                                                content: [
                                                    {
                                                        block: 'img',
                                                        src: './images/media/' +
                                                        '2.jpg',
                                                    },
                                                    {
                                                        elem: 'type',
                                                        elemMods: {video: true},
                                                        mix: {block: 'icon'},
                                                        cls: 'icon-triangle_' +
                                                        'right',
                                                    },
                                                ],
                                            },
                                            {
                                                elem: 'body',
                                                content: [
                                                    {
                                                        elem: 'date',
                                                        content: '24.05.2012',
                                                    },
                                                    {
                                                        elem: 'title',
                                                        content: 'Предметные ' +
                                                        'кружки для школьников',
                                                    },
                                                    {
                                                        elem: 'text',
                                                        content: 'Целью проек' +
                                                        'та является объедине' +
                                                        'ние усилий образоват' +
                                                        'ельных и общественны' +
                                                        'х организаций, орган' +
                                                        'ов исполнительной вл' +
                                                        'асти, заинтересованн' +
                                                        'ых в развитии одарён' +
                                                        'ных детей и талантли' +
                                                        'вой молодёжи, в подг' +
                                                        'отовке ее к созидате' +
                                                        'льной деятельности н' +
                                                        'а благо',
                                                    },
                                                ],
                                            },
                                        ],
                                    },
                                    {
                                        block: 'card',
                                        attrs: {
                                            'data-type': 'video',
                                            'data-toggle': 'modal',
                                        },
                                        content: [
                                            {
                                                elem: 'img',
                                                content: [
                                                    {
                                                        block: 'img',
                                                        src: './images/media/' +
                                                        '3.jpg',
                                                    },
                                                    {
                                                        elem: 'type',
                                                        elemMods: {video: true},
                                                        mix: {block: 'icon'},
                                                        cls: 'icon-triangle_' +
                                                        'right',
                                                    },
                                                ],
                                            },
                                            {
                                                elem: 'body',
                                                content: [
                                                    {
                                                        elem: 'date',
                                                        content: '24.05.2012',
                                                    },
                                                    {
                                                        elem: 'title',
                                                        content: 'Олимпийская' +
                                                        ' смена',
                                                    },
                                                    {
                                                        elem: 'text',
                                                        content: 'Организацио' +
                                                        'нно университетский ' +
                                                        'округ состоит из кла' +
                                                        'стеров (группы образ' +
                                                        'овательных организац' +
                                                        'ий общего, среднего ' +
                                                        'профессионального и ' +
                                                        'дополнительного обра' +
                                                        'зования), объединенн' +
                                                        'ых вокруг опорной ор' +
                                                        'ганизации, в роли ко' +
                                                        'торой может выступат' +
                                                        'ь школа, гимназия, л' +
                                                        'ицей',
                                                    },
                                                ],
                                            },
                                        ],
                                    },
                                    {
                                        block: 'card',
                                        attrs: {
                                            'data-type': 'video',
                                            'data-toggle': 'modal',
                                        },
                                        content: [
                                            {
                                                elem: 'img',
                                                content: [
                                                    {
                                                        block: 'img',
                                                        src: './images/media/' +
                                                        '1.jpg',
                                                    },
                                                    {
                                                        elem: 'type',
                                                        elemMods: {video: true},
                                                        mix: {block: 'icon'},
                                                        cls: 'icon-triangle_' +
                                                        'right',
                                                    },
                                                ],
                                            },
                                            {
                                                elem: 'body',
                                                content: [
                                                    {
                                                        elem: 'date',
                                                        content: '24.05.2012',
                                                    },
                                                    {
                                                        elem: 'title',
                                                        content: 'День ' +
                                                        'рождения ВолГУ',
                                                    },
                                                    {
                                                        elem: 'text',
                                                        content: 'В рамках ре' +
                                                        'ализации Концепции о' +
                                                        'бщенациональной сист' +
                                                        'емы выявления и разв' +
                                                        'ития молодых таланто' +
                                                        'в Волгоградский госу' +
                                                        'дарственный универси' +
                                                        'тет выступил с иници' +
                                                        'ативой создания в Во' +
                                                        'лгоградской области ' +
                                                        'комплексного проекта',
                                                    },
                                                ],
                                            },
                                        ],
                                    },
                                    {
                                        block: 'card',
                                        attrs: {
                                            'data-type': 'photo',
                                            'data-toggle': 'modal',
                                        },
                                        content: [
                                            {
                                                elem: 'img',
                                                content: [
                                                    {
                                                        block: 'img',
                                                        src: './images/media/' +
                                                        '2.jpg',
                                                    },
                                                    {
                                                        elem: 'type',
                                                        elemMods: {photo: true},
                                                        content: '114 фото',
                                                    },
                                                ],
                                            },
                                            {
                                                elem: 'body',
                                                content: [
                                                    {
                                                        elem: 'date',
                                                        content: '24.05.2012',
                                                    },
                                                    {
                                                        elem: 'title',
                                                        content: 'Предметные ' +
                                                        'кружки для школьников',
                                                    },
                                                    {
                                                        elem: 'text',
                                                        content: 'Целью проек' +
                                                        'та является объедине' +
                                                        'ние усилий образоват' +
                                                        'ельных и общественны' +
                                                        'х организаций, орган' +
                                                        'ов исполнительной вл' +
                                                        'асти, заинтересованн' +
                                                        'ых в развитии одарён' +
                                                        'ных детей и талантли' +
                                                        'вой молодёжи, в подг' +
                                                        'отовке ее к созидате' +
                                                        'льной деятельности н' +
                                                        'а благо',
                                                    },
                                                ],
                                            },
                                        ],
                                    },
                                    {
                                        block: 'card',
                                        attrs: {
                                            'data-type': 'photo',
                                            'data-toggle': 'modal',
                                        },
                                        content: [
                                            {
                                                elem: 'img',
                                                content: [
                                                    {
                                                        block: 'img',
                                                        src: './images/media/' +
                                                        '3.jpg',
                                                    },
                                                    {
                                                        elem: 'type',
                                                        elemMods: {photo: true},
                                                        content: '114 фото',
                                                    },
                                                ],
                                            },
                                            {
                                                elem: 'body',
                                                content: [
                                                    {
                                                        elem: 'date',
                                                        content: '24.05.2012',
                                                    },
                                                    {
                                                        elem: 'title',
                                                        content: 'Олимпийская' +
                                                        ' смена',
                                                    },
                                                    {
                                                        elem: 'text',
                                                        content: 'Организацио' +
                                                        'нно университетский ' +
                                                        'округ состоит из кла' +
                                                        'стеров (группы образ' +
                                                        'овательных организац' +
                                                        'ий общего, среднего ' +
                                                        'профессионального и ' +
                                                        'дополнительного обра' +
                                                        'зования), объединенн' +
                                                        'ых вокруг опорной ор' +
                                                        'ганизации, в роли ко' +
                                                        'торой может выступат' +
                                                        'ь школа, гимназия, л' +
                                                        'ицей',
                                                    },
                                                ],
                                            },
                                        ],
                                    },
                                    {
                                        block: 'card',
                                        attrs: {
                                            'data-type': 'photo',
                                            'data-toggle': 'modal',
                                        },
                                        content: [
                                            {
                                                elem: 'img',
                                                content: [
                                                    {
                                                        block: 'img',
                                                        src: './images/media/' +
                                                        '1.jpg',
                                                    },
                                                    {
                                                        elem: 'type',
                                                        elemMods: {photo: true},
                                                        content: '114 фото',
                                                    },
                                                ],
                                            },
                                            {
                                                elem: 'body',
                                                content: [
                                                    {
                                                        elem: 'date',
                                                        content: '24.05.2012',
                                                    },
                                                    {
                                                        elem: 'title',
                                                        content: 'День ' +
                                                        'рождения ВолГУ',
                                                    },
                                                    {
                                                        elem: 'text',
                                                        content: 'В рамках ре' +
                                                        'ализации Концепции о' +
                                                        'бщенациональной сист' +
                                                        'емы выявления и разв' +
                                                        'ития молодых таланто' +
                                                        'в Волгоградский госу' +
                                                        'дарственный универси' +
                                                        'тет выступил с иници' +
                                                        'ативой создания в Во' +
                                                        'лгоградской области ' +
                                                        'комплексного проекта',
                                                    },
                                                ],
                                            },
                                        ],
                                    },
                                    {
                                        block: 'card',
                                        attrs: {
                                            'data-type': 'photo',
                                            'data-toggle': 'modal',
                                        },
                                        content: [
                                            {
                                                elem: 'img',
                                                content: [
                                                    {
                                                        block: 'img',
                                                        src: './images/media/' +
                                                        '2.jpg',
                                                    },
                                                    {
                                                        elem: 'type',
                                                        elemMods: {photo: true},
                                                        content: '114 фото',
                                                    },
                                                ],
                                            },
                                            {
                                                elem: 'body',
                                                content: [
                                                    {
                                                        elem: 'date',
                                                        content: '24.05.2012',
                                                    },
                                                    {
                                                        elem: 'title',
                                                        content: 'Предметные ' +
                                                        'кружки для школьников',
                                                    },
                                                    {
                                                        elem: 'text',
                                                        content: 'Целью проек' +
                                                        'та является объедине' +
                                                        'ние усилий образоват' +
                                                        'ельных и общественны' +
                                                        'х организаций, орган' +
                                                        'ов исполнительной вл' +
                                                        'асти, заинтересованн' +
                                                        'ых в развитии одарён' +
                                                        'ных детей и талантли' +
                                                        'вой молодёжи, в подг' +
                                                        'отовке ее к созидате' +
                                                        'льной деятельности н' +
                                                        'а благо',
                                                    },
                                                ],
                                            },
                                        ],
                                    },
                                    {
                                        block: 'card',
                                        attrs: {
                                            'data-type': 'photo',
                                            'data-toggle': 'modal',
                                        },
                                        content: [
                                            {
                                                elem: 'img',
                                                content: [
                                                    {
                                                        block: 'img',
                                                        src: './images/media/' +
                                                        '3.jpg',
                                                    },
                                                    {
                                                        elem: 'type',
                                                        elemMods: {photo: true},
                                                        content: '114 фото',
                                                    },
                                                ],
                                            },
                                            {
                                                elem: 'body',
                                                content: [
                                                    {
                                                        elem: 'date',
                                                        content: '24.05.2012',
                                                    },
                                                    {
                                                        elem: 'title',
                                                        content: 'Олимпийская' +
                                                        ' смена',
                                                    },
                                                    {
                                                        elem: 'text',
                                                        content: 'Организацио' +
                                                        'нно университетский ' +
                                                        'округ состоит из кла' +
                                                        'стеров (группы образ' +
                                                        'овательных организац' +
                                                        'ий общего, среднего ' +
                                                        'профессионального и ' +
                                                        'дополнительного обра' +
                                                        'зования), объединенн' +
                                                        'ых вокруг опорной ор' +
                                                        'ганизации, в роли ко' +
                                                        'торой может выступат' +
                                                        'ь школа, гимназия, л' +
                                                        'ицей',
                                                    },
                                                ],
                                            },
                                        ],
                                    },
                                ],
                            },
                            {
                                block: 'btn',
                                mods: {secondary: true, more: true},
                                content: [
                                    'Еще материалы',
                                    {
                                        block: 'icon',
                                        tag: 'i',
                                        cls: 'icon-arr',
                                    },
                                ],
                            },
                            require('./common/pagination.bemjson.js'),
                        ],
                    },
                ],
            },
        },
        require('./common/slider_banners.bemjson.js'),
        require('./common/links.bemjson.js'),
        require('./common/footer.bemjson.js'),
        require('./common/modal_feedback.bemjson.js'),
        require('./common/modal_media.bemjson.js'),
    ],
};
