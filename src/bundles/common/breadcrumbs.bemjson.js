module.exports = {
    content: {
        cls: 'container',
        content: {
            block: 'breadcrumbs',
            content: [
                {
                    elem: 'item',
                    content: {
                        elem: 'link',
                        content: 'Главная',
                    },
                },
                {
                    elem: 'item',
                    content: {
                        elem: 'link',
                        content: 'Студенту',
                    },
                },
                {
                    elem: 'item',
                    content: {
                        elem: 'link',
                        elemMods: {selected: true},
                        content: 'Расписание зимней сессии 2018',
                    },
                },
            ],
        },
    },
};
