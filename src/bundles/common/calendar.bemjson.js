var dayName = ['ПН', 'ВТ', 'СР', 'ЧТ', 'ПТ', 'СБ', 'ВС'];

module.exports = {
    block: 'calendar',
    content: [
        {
            elem: 'triangle',
        },
        {
            elem: 'header',
            content: [
                {
                    elem: 'month',
                    attrs: {'data-month': '05'},
                    content: 'Май',
                },
                {
                    elem: 'year',
                    attrs: {'data-year': '17'},
                    content: '2017',
                },
            ],
        },
        {
            elem: 'days',
            content: [(function() {
                var res = [];
                var tmp = [];
                var dayTxt = '';

                for(var i=0; i<dayName.length; i++) {
                    res.push({
                        elem: 'day',
                        elemMods: {letter: true},
                        content: dayName[i],
                    });
                }

                for(i=28; i<31; i++) {
                    tmp.push({
                        elem: 'day',
                        elemMods: {disabled: true, number: true},
                        content: i,
                    });
                }

                for(i=1; i<=31; i++) {
                    dayTxt = (i < 10) ? '0' + i : i;
                    tmp.push({
                        elem: 'day',
                        elemMods: {number: true},
                        attrs: {'data-day': dayTxt},
                        content: i,
                    });
                }

                tmp.push({
                    elem: 'day',
                    elemMods: {disabled: true, number: true},
                    content: 1,
                });

                res.push({
                    elem: 'numbers',
                    content: [tmp],
                });

                return res;
            })()],
        },
    ],
};
