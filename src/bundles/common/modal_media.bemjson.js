var images = ['11', '12', '7', '4', '2', '9', '8', '3', '10', '5'];

module.exports = {
    block: 'modal',
    mods: {media: true},
    attrs: {
        tabindex: -1,
        role: 'dialog',
    },
    content: {
        cls: 'container',
        content: {
            elem: 'dialog',
            attrs: {role: 'document'},
            content: {
                elem: 'content',
                content: [
                    {
                        elem: 'header',
                        content: [
                            {
                                block: 'mark',
                                content: '19.10.16',
                            },
                            {
                                elem: 'title',
                                content: 'ВолГУ РЯДом конец второго ' +
                                'образовательного модуля ',
                            },
                            {
                                block: 'btn',
                                mods: {transparent: true},
                                attrs: {'data-dismiss': 'modal'},
                                content: {
                                    block: 'icon',
                                    tag: 'i',
                                    cls: 'icon-close-win',
                                },
                            },
                        ],
                    },
                    {
                        elem: 'body',
                        elemMods: {photo: true},
                        content: {
                            block: 'photogallery',
                            content: [
                                {
                                    elem: 'wrapper',
                                    elemMods: {view: true},
                                    content: [
                                        {
                                            block: 'icon',
                                            tag: 'i',
                                            cls: 'icon-arr_left',
                                        },
                                        {
                                            elem: 'box',
                                            elemMods: {view: true},
                                            content: [
                                                {
                                                    elem: 'photo',
                                                    content: [
                                                        {
                                                            elem: 'img-wrapper',
                                                            content: [
                                                                {
                                                                    block: 'img',
                                                                    src: 'images/photogallery/1.jpg',
                                                                },
                                                            ],
                                                        },
                                                        {
                                                            elem: 'counter',
                                                            content: [
                                                                {
                                                                    elem: 'cnt-num',
                                                                    elemMods: {
                                                                        cur: true,
                                                                    },
                                                                    content: '',
                                                                },
                                                                '/',
                                                                {
                                                                    elem: 'cnt-num',
                                                                    elemMods: {
                                                                        all: true,
                                                                    },
                                                                    content: '',
                                                                },
                                                            ],
                                                        },
                                                    ],
                                                },
                                                {
                                                    elem: 'descriptions',
                                                    content: {
                                                        elem: 'descr-wrapper',
                                                        content: [
                                                            {
                                                                elem: 'descr',
                                                                content: '',
                                                            },
                                                            {
                                                                elem: 'share',
                                                                content: [
                                                                    {
                                                                        block: 'icon',
                                                                        cls: 'icon-share',
                                                                    },
                                                                    {
                                                                        block: 'btn',
                                                                        mods: {
                                                                            secondary: true,
                                                                            share: true,
                                                                        },
                                                                        content: 'Поделиться',
                                                                    },
                                                                    require('./share.bemjson.js'),
                                                                ],
                                                            },
                                                        ],
                                                    },
                                                },
                                            ],
                                        },
                                        {
                                            block: 'icon',
                                            tag: 'i',
                                            cls: 'icon-arr_right',
                                        },
                                    ],
                                },
                                {
                                    elem: 'wrapper',
                                    elemMods: {grid: true},
                                    mix: {block: 'slider', mods: {gallery: true}},
                                    content: [
                                        {
                                            block: 'icon',
                                            tag: 'i',
                                            cls: 'icon-arr_left',
                                        },
                                        {
                                            elem: 'box',
                                            attrs: {'data-slides': images.length},
                                            elemMods: {grid: true},
                                            content: [(function() {
                                                var res = [];
                                                var tmp = [];
                                                var imgPath = '';
                                                var eMods = '';
                                                var j = 0;

                                                for(var i=0; i<images.length; i++) {
                                                    if(j === 0) {
                                                        tmp.push({
                                                            elem: 'slide',
                                                            content: [],
                                                        });
                                                    }

                                                    imgPath = 'images/' +
                                                        'photogallery/' +
                                                        images[i];

                                                    eMods = (i === 0) ? 'photogallery__current' : '';

                                                    tmp[0].content.push({
                                                        block: 'link',
                                                        cls: eMods,
                                                        attrs: {
                                                            'data-img': imgPath +
                                                            '_big.jpg',
                                                            'data-descr': 'Итоговое ' +
                                                            'мероприятие ' +
                                                            '«Фестиваль русского' +
                                                            ' языка и культуры»' +
                                                            ' проекта «ВолГУ ' +
                                                            'РЯдом» прошло в ' +
                                                            'здании научной ' +
                                                            'библиотеки ' +
                                                            'Волгоградского ' +
                                                            'государственного ' +
                                                            'университета.',
                                                            'data-slide': i+1,
                                                        },
                                                        content: {
                                                            block: 'img',
                                                            src: imgPath + '.jpg',
                                                        },
                                                    });

                                                    j++;

                                                    if(j === 2) {
                                                        res.push(tmp);
                                                         j = 0;
                                                        tmp = [];
                                                    }
                                                }
                                                res.push(tmp);
                                                return res;
                                            })()],
                                        },
                                        {
                                            block: 'icon',
                                            tag: 'i',
                                            cls: 'icon-arr_right',
                                        },
                                    ],
                                },
                            ],
                        },
                    },
                    {
                        elem: 'body',
                        elemMods: {video: true},
                        content: {
                            block: 'photogallery',
                            content: {
                                elem: 'wrapper',
                                content: {
                                    elem: 'box',
                                    content: [
                                        {
                                            block: 'iframe',
                                            src: 'https://www.youtube.com/embed/jHdSPmIfO5U',
                                        },
                                        {
                                            elem: 'descr-wrapper',
                                            content: [
                                                {
                                                    elem: 'descr',
                                                    content: 'ИИтоговое мероприятие ' +
                                                    '«Фестиваль русского языка и ' +
                                                    'культуры» проекта «ВолГУ РЯдом» ' +
                                                    'прошло в здании научной библиотеки ' +
                                                    'Волгоградского государственного ' +
                                                    'университета.',
                                                },
                                                {
                                                    elem: 'share',
                                                    content: [
                                                        {
                                                            block: 'icon',
                                                            cls: 'icon-share',
                                                        },
                                                        {
                                                            block: 'btn',
                                                            mods: {
                                                                secondary: true,
                                                                share: true,
                                                            },
                                                            content: 'Поделиться',
                                                        },
                                                        require('./share.bemjson.js'),
                                                    ],
                                                },
                                            ],
                                        },
                                    ],
                                },
                            },
                        },
                    },
                ],
            },
        },
    },
};
