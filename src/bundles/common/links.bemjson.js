var files = ['university', 'education', 'science', 'media',
    'intelligence', 'reference'];

module.exports = {
    block: 'links-replace',
    content: {
        cls: 'container',
        content: {
            block: 'links',
            content: [(function() {
                var res = [];

                for(var i=0; i<files.length; i++) {
                    res.push( require('./links_' + files[i] + '.bemjson.js') );
                }

                return res;
            })()],
        },
    },
};
