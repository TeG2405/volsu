var items = ['Факультеты и институты', 'Учебное управление',
    'Управление нового набора', 'Отдел платных образовательных услуг',
    'Центр совместных международных образовательных программ',
    'Центр развития качества образования',
    'Центр общественно-профессиональной и международной аккредитации ' +
    'образовательных программ', 'Бакалавриат', 'Специалитет', 'Магистратура',
    'Аспирантура'];

module.exports = {
    block: 'sidebar',
    content: [
        {
            block: 'sidebar-menu',
            content: [
                {
                    elem: 'header-wrapper',
                    content: [
                        {
                            elem: 'header',
                            content: 'Студенту',
                        },
                        {
                            block: 'btn',
                            mods: {transparent: true},
                            content: [
                                {elem: 'bar'},
                                {elem: 'bar'},
                                {elem: 'bar'},
                            ],
                        },
                    ],
                },
                {
                    elem: 'menu-content',
                    elemMods: {collapse: true},
                    content: [
                        {
                            elem: 'menu',
                            content: [(function() {
                                var res = [];

                                for(var i=0; i<items.length; i++) {
                                    res.push({
                                        elem: 'item',
                                        content: {
                                            block: 'link',
                                            content: items[i],
                                        },
                                    });
                                }

                                return res;
                            })()],
                        },
                        {
                            elem: 'blocks-wrapper',
                            content: [
                                {
                                    block: 'link-block',
                                    content: [
                                        {
                                            block: 'img',
                                            src: 'images/link-img/sdo.jpg',
                                        },
                                        {
                                            elem: 'link',
                                            content: 'Система дистанционного образования' +
                                            ' «Умка»',
                                        },
                                    ],
                                },
                                {
                                    block: 'link-block',
                                    content: [
                                        {
                                            block: 'img',
                                            src: 'images/link-img/' +
                                            'library.jpg',
                                        },
                                        {
                                            elem: 'link',
                                            content: 'Библиотека ВолГУ',
                                        },
                                    ],
                                },
                            ],
                        },
                    ],
                },
            ],
        },
    ],
};
