module.exports = {
    block: 'modal',
    mods: {feedback: true},
    attrs: {
        tabindex: -1,
        role: 'dialog',
    },
    content: {
        cls: 'container',
        content: {
            elem: 'dialog',
            attrs: {role: 'document'},
            content: {
                elem: 'content',
                content: [
                    {
                        elem: 'header',
                        content: [
                            {
                                elem: 'title',
                                content: 'Задайте интересующий Вас вопрос',
                            },
                            {
                                block: 'btn',
                                mods: {transparent: true},
                                attrs: {'data-dismiss': 'modal'},
                                content: {
                                    block: 'icon',
                                    tag: 'i',
                                    cls: 'icon-close-win',
                                },
                            },
                        ],
                    },
                    {
                        elem: 'body',
                        content: {
                            block: 'form',
                            content: [
                                {
                                    mix: [
                                        {block: 'form-group'},
                                        {block: 'row'},
                                    ],
                                    content: [
                                        {
                                            elem: 'label',
                                            elemMods: {required: true},
                                            content: 'ФИО',
                                            for: 'fio',
                                            mix: {block: 'col-xl-3'},
                                        },
                                        {
                                            block: 'input',
                                            mix: [
                                                {block: 'form-control'},
                                                {block: 'col-xl-9'},
                                            ],
                                            name: 'fio',
                                        },
                                    ],
                                },
                                {
                                    mix: [
                                        {block: 'form-group'},
                                        {block: 'row'},
                                    ],
                                    content: [
                                        {
                                            elem: 'label',
                                            elemMods: {required: true},
                                            content: 'E-mail',
                                            for: 'mail',
                                            mix: {block: 'col-xl-3'},
                                        },
                                        {
                                            block: 'input',
                                            mix: [
                                                {block: 'form-control'},
                                                {block: 'col-xl-9'},
                                            ],
                                            name: 'mail',
                                        },
                                    ],
                                },
                                {
                                    mix: [
                                        {block: 'form-group'},
                                        {block: 'row'},
                                    ],
                                    content: [
                                        {
                                            elem: 'label',
                                            content: 'Телефон',
                                            for: 'phone',
                                            mix: {block: 'col-xl-3'},
                                        },
                                        {
                                            block: 'input',
                                            mix: [
                                                {block: 'form-control'},
                                                {block: 'col-xl-9'},
                                            ],
                                            name: 'phone',
                                        },
                                    ],
                                },
                                {
                                    mix: [
                                        {block: 'form-group'},
                                        {block: 'row'},
                                        {block: 'align-items-start'},
                                    ],
                                    content: [
                                        {
                                            elem: 'label',
                                            elemMods: {required: true},
                                            content: 'Сообщение',
                                            for: 'msg',
                                            mix: {block: 'col-xl-3'},
                                        },
                                        {
                                            block: 'textarea',
                                            mix: [
                                                {block: 'form-control'},
                                                {block: 'col-xl-9'},
                                            ],
                                            name: 'msg',
                                        },
                                    ],
                                },
                                {
                                    mix: [
                                        {block: 'form-group'},
                                        {block: 'row'},
                                    ],
                                    content: {
                                        elem: 'buttons',
                                        mix: [
                                            {block: 'col-xl-9'},
                                            {block: 'offset-xl-3'},
                                        ],
                                        content: {
                                            block: 'btn',
                                            mods: {secondary: true, send: true},
                                            content: 'Отправить',
                                        },
                                    },
                                },
                            ],
                        },
                    },
                ],
            },
        },
    },
};
