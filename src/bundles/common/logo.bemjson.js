module.exports = {
    block: 'logo',
    content: [
        {
            elem: 'image',
            src: 'images/logo.png',
        },
    ],
};
