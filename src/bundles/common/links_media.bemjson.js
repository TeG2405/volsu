var links = ['Фотогалерея', 'Видеоматериалы'];

module.exports = {
    elem: 'list',
    content: [
        {
            elem: 'item',
            content: {
                elem: 'link',
                elemMods: {'header-list': true},
                content: 'Медиа',
            },
        },

        (function() {
            var res = [];

            for(var i=0; i<links.length; i++) {
                res.push({
                    elem: 'item',
                    content: {
                        elem: 'link',
                        content: links[i],
                    },
                });
            }

            return res;
        })(),
    ],
};
