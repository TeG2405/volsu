module.exports = function(bh) {
    bh.match('input', function(ctx, json) {
        ctx.tag('input');
        ctx.attrs({
            type: 'text',
            name: json.name,
            id: json.name,
        });
    });
};

