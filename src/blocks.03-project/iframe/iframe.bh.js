module.exports = function(bh) {
    bh.match('iframe', function(ctx, json) {
        ctx.tag('iframe');
        ctx.attrs({src: json.src});
    });
};

