module.exports = function(bh) {
    bh.match('triangle_size_sm', function(ctx, json) {
        ctx.html('<svg xmlns="http://www.w3.org/2000/svg" ' +
            'xmlns:xlink="http://www.w3.org/1999/xlink" width="41px" ' +
            'height="39px"><defs><pattern id="img' + json.id + '" ' +
            'patternUnits="userSpaceOnUse" width="41" height="39">' +
            '<image xlink:href="' + json.src + '" x="0" y="0" height="39" ' +
            'width="41"/></pattern></defs>' +
            '<path fill="url(#img' + json.id + ')"  ' +
            'stroke="rgb(235, 235, 235)" ' +
            'stroke-width="1px" stroke-linecap="butt" ' +
            'stroke-linejoin="miter" ' +
            'd="M2.423,5.855 C4.308,5.552 37.397,0.240 38.698,0.031 ' +
            'C40.384,-0.240 41.639,1.307 40.653,3.193 C39.863,4.704 ' +
            '23.915,34.932 22.551,37.512 C21.655,39.207 19.519,39.524 ' +
            '18.622,38.143 C17.621,36.604 1.593,11.404 0.479,9.571 ' +
            'C-0.337,8.230 0.565,6.153 2.423,5.855 Z"/></svg>');
    });
};
