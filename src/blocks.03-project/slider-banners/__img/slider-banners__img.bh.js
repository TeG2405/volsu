module.exports = function(bh) {
    bh.match('slider-banners__img', function(ctx, json) {
        ctx.tag('img');
        ctx.attrs({
            src: json.src,
            alt: '',
            title: '',
        });
    });
};

