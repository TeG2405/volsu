module.exports = function(bh) {
    bh.match('navigation__link', function(ctx) {
        ctx.tag('a');
        ctx.attrs({href: '#'});
    });
};

