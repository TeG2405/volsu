module.exports = function(bh) {
    bh.match('news__item', function(ctx, json) {
        ctx.tag('a');
        ctx.attrs({
            'href': '#',
            'data-filter': json.filter,
        });
    });
};

