module.exports = function(bh) {
    bh.match('news__photo', function(ctx, json) {
        ctx.tag('img');
        ctx.attrs({
            src: json.src,
            alt: '',
            title: '',
        });
    });
};

