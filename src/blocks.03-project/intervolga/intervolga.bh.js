module.exports = function(bh) {
    bh.match('intervolga', function(ctx, json) {
        ctx.tag('a');
        ctx.attrs({
            href: 'https://www.intervolga.ru/',
            target: '_blank',
        });
        ctx.html('<img src="images/intervolga.png" alt="INTERVOLGA" ' +
            'title="Интернет-агентство &laquo;Интерволга&raquo;">');
    });
};

