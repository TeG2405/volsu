module.exports = function(bh) {
    bh.match('links__link', function(ctx, json) {
        ctx.tag('a');
        if(!ctx.ctx.elemMods['header-list']) {
            ctx.attrs({href: '#'});
        }
    });
};

