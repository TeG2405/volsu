module.exports = function(bh) {
    bh.match('mark', function(ctx, json) {
        if(json.type === 'link') {
            ctx.tag('a');
            ctx.attrs({'href': '#'});
        } else {
            ctx.tag('span');
        }
    });
};

