$(document).ready(function() {
    /**
     * Highlight tags on cards
     *
     * This function must be deleted if send AJAX-request or otherwise.
     * This function only for test!
     *
     * @param tag string
     */
    function tagsOnCards(tag) {
        $('.main__grid .mark').filter(function() {
            if($(this).data('tag') === tag) {
                $(this).toggleClass('mark_selected');
            }
        });
    }

    $('.tags').on('click', '.link, .icon-close', function(event) {
        event.preventDefault();
        $(this).parents('.tags__item').toggleClass('tags__item_selected');

        tagsOnCards($(this).parents('.tags__item').data('tag'));
    });
});
