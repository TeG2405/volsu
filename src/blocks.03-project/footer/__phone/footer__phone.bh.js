module.exports = function(bh) {
    bh.match('footer__phone', function(ctx, json) {
        ctx.tag('a');
        ctx.attrs({'href': 'tel:'+json.tel});
    });
};

