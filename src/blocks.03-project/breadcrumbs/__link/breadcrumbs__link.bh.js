module.exports = function(bh) {
    bh.match('breadcrumbs__link', function(ctx, json) {
        if(json.elemMods.selected) {
            ctx.tag('span');
        } else {
            ctx.tag('a');
            ctx.attrs({href: '#'});
        }
    });
};

