module.exports = function(bh) {
    bh.match('filter__control', function(ctx, json) {
        ctx.tag('span');
        ctx.attrs({'data-filter': json.filter});
    });
};

