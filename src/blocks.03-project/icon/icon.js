$(document).ready(function() {
    /*
    * Функция для поиска текста на сайте
    **/
    function ajaxSearchText(text) {
        alert('Search `' + text + '`');
    }

    var inputHiddenClass = 'input_hidden';

    $('.settings').on('click', '.icon-search', function() {
        var parent = $(this).parents('.settings__search');
        var input = parent.find('.input_search');
        var zIndex = input.css('z-index');
        var searchText = input.val();

        if(zIndex < 0) {
            input.show().removeClass(inputHiddenClass);
            parent.addClass('search_show');
        } else {
            ajaxSearchText(searchText);
        }
    });

    $('.input_search').keypress(function(e) {
        if(e.which == 13) {
            ajaxSearchText( $(this).val() );
        }
    });

    $('body').on('click', function(elem) {
        var parent = $('.settings__search');
        var input = parent.find('.input_search');

        if((!elem.target.classList.contains('icon-search')) &&
           (!elem.target.classList.contains('input_search')) &&
           (input.css('z-index') > 0)) {
                parent.removeClass('search_show');
                input.addClass(inputHiddenClass);
        }
    });
});
