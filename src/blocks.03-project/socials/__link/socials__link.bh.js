module.exports = function(bh) {
    bh.match('socials__link', function(ctx, json) {
        ctx.tag('a');
        ctx.attrs({
            href: '#',
            target: '_blank',
        });
    });
};

