$(document).ready(function() {
    function clearCurrentClass() {
        $('.photogallery').find(dotCurrentClass).removeClass(currentClass);
    }

    function setSlideInfo(info) {
        var gallery = $('.photogallery');

        // Set attributes
        gallery.find('.photogallery__img-wrapper img').attr('src', info.img);
        gallery.find('.photogallery__descr').text( info.descr );
        // Set counter
        gallery.find('.photogallery__cnt-num_cur').text( info.slide );
    }

    var currentClass = 'photogallery__current';
    var dotCurrentClass = '.' + currentClass;

    $('.photogallery').on('click', function(event) {
        var target = event.target;
        var gallery = $(this);

        if($(target).hasClass('img')) {
            event.preventDefault();

            var link = $(target).parents('.link');
            var pathBigImg = link.data('img');
            var descr = link.data('descr');
            var slideNum = link.data('slide');

            // Set attributes
            setSlideInfo({
                img: pathBigImg,
                descr: descr,
                slide: slideNum,
            });

            // Set current clas to clicked image
            clearCurrentClass();
            link.addClass(currentClass);
        }
    });

    $('.photogallery__wrapper_view').on('click', function(elem) {
        var target = elem.target;
        var isRight = $(target).hasClass('icon-arr_right');

        if(isRight || $(target).hasClass('icon-arr_left')) {
            var current = $(dotCurrentClass);
            var curSlide = parseInt(current.data('slide'));
            var allSlides = $('.photogallery__box_grid').data('slides');
            var nextSlideNum = 0;

            clearCurrentClass();

            if(curSlide === allSlides) {
                nextSlideNum = (isRight) ? 1 : (curSlide - 1);
            } else {
                if((curSlide === 1) && (!isRight)) {
                    nextSlideNum = allSlides;
                } else {
                    nextSlideNum = (isRight) ? (curSlide + 1) : (curSlide - 1);
                }
            }

            var nextSlide = $('.photogallery__box_grid .link[data-slide="' +
                nextSlideNum + '"]');

            setSlideInfo( nextSlide.data() );
            nextSlide.addClass(currentClass);
        }
    });
});
