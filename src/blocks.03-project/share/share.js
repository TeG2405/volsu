$(document).ready(function() {
    $('.share').on('click', '.link', function(event) {
        event.preventDefault();

        window.open(
            this.href,
            this.title,
            'width=600,height=400,toolbar=0,status=0'
        );

        $('.share').toggleClass('share_hide');
        return false;
    });
});
