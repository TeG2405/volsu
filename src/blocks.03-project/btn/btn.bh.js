module.exports = function(bh) {
    bh.match('btn', function(ctx, json) {
        ctx.tag('button');

        var toggle = null;
        var target = null;

        if(json.toggle) {
            toggle = json.toggle;
        }

        if(json.target) {
            target = json.target;
        }

        ctx.attrs({
            'data-toggle': toggle,
            'data-target': target,
        });
    });
};

