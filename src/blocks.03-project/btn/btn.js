$(document).ready(function() {
    /*
    * Кнопка "Еще материалы"
    **/
    function getDataFromServer() {
        alert('Get data from the server');
    }
    /*
    * Показывает/скрывает меню со ссылками в шапке
    **/
    function toggleLinks() {
        var btn = $('.header__menu .btn_transparent');
        var links = $('.links-replace');
        var linksOpenClass = 'links-replace_top';
        var navWrapper = $('.navigation-wrapper_style_full-length');

        if(!btn.hasClass('btn_close')) {
            var top = ($(window).width() > 991) ?
                    btn.parents('.header__controls').outerHeight(true) :
                    btn.parents('.header__menu').outerHeight(true);

            if(navWrapper.length > 0) {
                top += navWrapper.outerHeight(true);
            }

            var browsehappy = $('.page__browsehappy');

            if(browsehappy.is(':visible')) {
                top += browsehappy.outerHeight(true);
            }
        }

        btn.toggleClass('btn_close');

        links
            .css({opacity: 0})
            .toggleClass(linksOpenClass);

        if(btn.hasClass('btn_close')) {
            links.css({top: top + 'px'});
        }

        links.css({opacity: 1});
    }
    /*
    * Показывает/скрывает меню навигации на малых разрешениях экрана
    **/
    function toggleNavMenu() {
        var navWrapper = $('.navigation-wrapper');
        var btn = navWrapper.find('.btn_transparent');
        var nav = navWrapper.find('.navigation__list').parents('.navigation');

        btn.toggleClass('btn_close');
        nav.toggleClass('navigation_collapse');
    }
    /*
    * Показывает/скрывает боковую панель на малых разрешениях экрана
    **/
    function toggleSidebarMenu() {
        var header = $('.sidebar-menu__header-wrapper');
        var btn = header.find('.btn.btn_transparent');
        var menu = $('.sidebar-menu__menu-content');

        btn.toggleClass('btn_close');
        header.toggleClass('sidebar-menu__header-wrapper_open');
        menu.toggleClass('sidebar-menu__menu-content_collapse');
    }

    $('.header__menu').on('click', '.btn.btn_transparent', function() {
        toggleLinks();
    });

    $('.navigation__header').on('click', '.btn.btn_transparent', function() {
        toggleNavMenu();
    });

    $('.sidebar-menu__header-wrapper')
        .on('click', '.btn.btn_transparent', function() {
            toggleSidebarMenu();
        });

    $('.footer').on('click', '.btn_feedback', function() {
        $('.modal_feedback').modal('show');
    });

    $('.tags__subscribe').on('click', '.btn_subscribe', function(event) {
        var mail = $('.tags__subscribe').find('.input_mail-subscribe').val();

        if(mail.length === 0) {
            event.preventDefault();
        }
    });

    $('.btn_calendar')
        .on('click', '.calendar__day_number', function() {
            var cls = 'calendar__day_selected';
            var dotCls = '.' + cls;
            var calendar = $(this).parents('.calendar');
            var btnSpan = $(this).parents('.btn_calendar').find('.btn__text');
            var numbers = calendar.find('.calendar__numbers');
            var month = calendar.find('.calendar__month');
            var year = calendar.find('.calendar__year');

            numbers.find(dotCls).toggleClass(cls);
            $(this).toggleClass(cls);

            var txt = ($(this).parents('.calendar-from').length > 0) ? 'С' : 'ПО';

            btnSpan.text( txt + ' ' +
                $(this).data('day') + '.' +
                month.data('month') + '.' +
                year.data('year') );

            calendar.removeClass('calendar_show');
        })
        .on('click', function(elem) {
            var calendar = $(this).find('.calendar');
            var calendarShowClass = 'calendar_show';

            if(!($(elem.target).parents('.calendar').length > 0) &&
                !($(elem.target).hasClass('calendar'))) {
                if(!calendar.hasClass(calendarShowClass)) {
                    $('.calendar').removeClass(calendarShowClass);
                    calendar.toggleClass(calendarShowClass);
                } else {
                    $('.calendar').removeClass(calendarShowClass);
                }
            }
        });

    $('body')
        .on('click', function(elem) {
            if(!($(elem.target).hasClass('btn_transparent')) &&
                ($(elem.target).parents('.navigation').length === 0)) {
                var navWrapper = $('.navigation-wrapper'),
                    navCollapseStatus = navWrapper.find('.navigation_collapse').length;

                if(navCollapseStatus === 0) {
                    toggleNavMenu();
                }
            }

            if(!($(elem.target).hasClass('btn_transparent')) &&
                ($(elem.target).parents('.sidebar-menu').length === 0)) {
                var sbMenuStatus = $('.sidebar-menu__menu-content_collapse').length;

                if(sbMenuStatus === 0) {
                    toggleSidebarMenu();
                }
            }

            if(($(elem.target).parents('.header__menu').length === 0) &&
                ($(elem.target).parents('.links-replace').length == 0) &&
                ($('.links-replace').hasClass('links-replace_top'))) {
                toggleLinks();
            }

            if(($(elem.target).parents('.share').length === 0) &&
                !($(elem.target).hasClass('share')) &&
                !($(elem.target).hasClass('btn_share'))) {
                $('.share').addClass('share_hide');
            }

            if(($(elem.target).parents('.btn_calendar').length === 0) &&
                (!$(elem.target).hasClass('btn_calendar')) &&
                (!$(elem.target).hasClass('calendar'))){
                $('.calendar').removeClass('calendar_show');
            }
        })
        .on('click', '.btn_sort', function() {
            var sort = $(this).data('sort');

            if(location.search.indexOf(sort) === -1) {
                window.location.href = location.protocol +
                    '//' +
                    location.host +
                    location.pathname +
                    '?sort=' + sort;
            }
        })
        .on('click', '.btn_more', function() {
            getDataFromServer();
        })
        .on('click', '.btn_send', function(event) {
            event.preventDefault();

            var str = $('.modal_feedback form').serialize();

            alert(str);
        })
        .on('click', '.btn_share', function() {
            $('.share').toggleClass('share_hide');
        });
});
